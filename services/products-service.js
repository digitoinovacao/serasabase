'use strict';

var BaseA = require('../models/base-a');
var BaseB = require('../models/base-b');
var BaseC = require('../models/base-c');

async function findByA(cpf) {
    return new Promise(async (resolve, reject) => {
        try {
            let prod = await BaseA.findOne({
                where: { cpf: cpf },
                attributes: ["cpf", "name", "address", "createdAt", "updatedAt"],
            });
            resolve(prod);
        } catch (error) {
            reject(error);
        }
    });
}

async function findByB(age) {
    return new Promise(async (resolve, reject) => {
        try {
            let prod = await BaseB.findOne({
                where: { age: parseInt(age) },
                attributes: ["age", "listGoods", "address", "sourceIncome", "createdAt", "updatedAt"],
            });
            resolve(prod);
        } catch (error) {
            reject(error);
        }
    });
}

async function findByC(cpf) {

    return new Promise(async (resolve, reject) => {
        try {
            let prod = await BaseC.findOne({
                where: { cpf: cpf },
                attributes: ["cpf", "createdAt", "updatedAt"],
            });

            resolve(prod);

        } catch (error) {
            reject(error);
        }
    });
}

module.exports = {
    findByA: findByA,
    findByB: findByB,
    findByC: findByC
};