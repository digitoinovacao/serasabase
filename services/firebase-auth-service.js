var firebase = require('firebase/app');
require('firebase/auth');

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyDObR3IKwFtk5WzLSBqvPwl2s1t0cZjaB4",
    authDomain: "serasabase.firebaseapp.com",
    projectId: "serasabase",
    storageBucket: "serasabase.appspot.com",
    messagingSenderId: "742830327880",
    appId: "1:742830327880:web:c0f1b63e04c4c88862f07d",
    measurementId: "G-PWBD9T1XE7"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const auth = firebase.auth

module.exports = {
    firebase,
    auth
}