var express = require("express");
var router = express.Router();
const userController = require("../controller/users-controller");

router.post(
    "/login", userController.authenticate,
);

module.exports = router;