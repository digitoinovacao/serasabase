"use strict";
/**
 * Class de rota para cliente
 *
 *
 * @author Developed by Antônio Gusmão
 * @email antonio.gusmao@digitoinovacao.com.br
 * @site https://digitoinovacao.com.br
 *
 */
var express = require("express");
var router = express.Router();
const authorize = require("../helpers/authorize");

const usersController = require("../controller/users-controller");

router.post(
    "/",
    usersController.validate("register"),
    usersController.register
);

router.put(
    "/:uid",
    authorize.checkIfAuthenticated,
    usersController.validate("update"),
    usersController.update
);

router.get("/", authorize.checkIfAuthenticated, usersController.findByUid);

module.exports = router;