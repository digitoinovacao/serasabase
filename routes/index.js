var express = require('express');
var router = express.Router();
const version = require("../package.json").version;

/* GET home page. */
router.get('/info', function(req, res, next) {
    res.send({ message: "Versão: " + version });
});

module.exports = router;