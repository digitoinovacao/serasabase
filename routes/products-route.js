"use strict";
/**
 * Class de rota para cliente
 *
 *
 * @author Developed by Antônio Gusmão
 * @email antonio.gusmao@digitoinovacao.com.br
 * @site https://digitoinovacao.com.br
 *
 */
var express = require("express");
var router = express.Router();
const authorize = require("../helpers/authorize");

const productsController = require("../controller/products-controller");

router.post("/a", authorize.checkIfAuthenticated, productsController.findByA);
router.post("/b", authorize.checkIfAuthenticated, productsController.findByB);
router.post("/c", authorize.checkIfAuthenticated, productsController.findByC);

module.exports = router;