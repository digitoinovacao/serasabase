const { check, validationResult } = require("express-validator");
const productService = require("../services/products-service");

let findByA = async (req, res) => {

    try {

        const data = Object.assign({}, req.body) || {};

        const cpf = data.cpf;

        const prod = await productService.findByA(cpf);
        if (prod) {
            res.status(200).send(prod);
        } else {
            res.status(200).send({ 'message': "Nenhum registro encontrado." });
        }

    } catch (error) {
        console.error(error);
        if (error.message) {
            res.status(404).json({
                message: `Problemas ao consulta o registro: ${error.message}`,
            });
        }
        res.status(404).json({ message: ` ${error} ` });
    }
};

let findByB = async (req, res) => {
    try {
        const data = Object.assign({}, req.body) || {};

        const age = data.age;

        const prod = await productService.findByB(age);

        if (prod) {
            res.status(200).send(prod);
        } else {
            res.status(200).send({ 'message': "Nenhum registro encontrado." });
        }
    } catch (error) {
        console.error(error);
        if (error.message) {
            res.status(404).json({
                message: `Problemas ao consulta o registro: ${error.message}`,
            });
        }
        res.status(404).json({ message: ` ${error} ` });
    }
};

let findByC = async (req, res) => {
    try {

        const data = Object.assign({}, req.body) || {};

        const cpf = data.cpf;

        const prod = await productService.findByC(cpf);

        if (prod) {
            res.status(200).send(prod);
        } else {
            res.status(200).send({ 'message': "Nenhum registro encontrado." });
        }
    } catch (error) {
        console.error(error);
        if (error.message) {
            res.status(404).json({
                message: `Problemas ao consulta o registro: ${error.message}`,
            });
        }
        res.status(404).json({ message: ` ${error} ` });
    }
};

module.exports = {
    findByA: findByA,
    findByB: findByB,
    findByC: findByC
};