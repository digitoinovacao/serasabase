const { check, validationResult } = require("express-validator");
const mailer = require("../helpers/mailer");
var admin = require('../utils/fb');
var { auth } = require('../services/firebase-auth-service');

let authenticate = async (req, res) => {

    const data = Object.assign({}, req.body) || {};

    try {

        // Verica erros enviados pelo express-validator
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            res.status(422).json({ errors: errors.mapped() });
            return;
        }

        const user = await auth().signInWithEmailAndPassword(data.email, data.password);

        res.status(200).json(user);

    } catch (error) {
        console.error(error);
        if (error.message) {
            res
                .status(404)
                .json({ message: `${error.message}` });
        } else {
            res.status(404).json({ message: ` ${error} ` });
        }
    }
};

let register = async (req, res) => {

    const data = Object.assign({}, req.body) || {};

    try {
        // Verica erros enviados pelo express-validator
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            res.status(422).json({ errors: errors.mapped() });
            return;
        }

        const user = await admin.auth().createUser({
            email: data.email,
            password: data.password,
            displayName: data.displayName
        });

        if (user) {
            res.status(200).send({ message: 'Usuário registrado com sucesso.' });
        } else {
            res.status(401).send({ message: 'Problemas ao cadastro do usuário!' })
        }
    } catch (error) {
        console.error(error);
        if (error.message) {
            res
                .status(404)
                .json({ message: `${error.message}` });
        } else {
            res.status(404).json({ message: ` ${error} ` });
        }
    }
};

let update = async (req, res) => {
    try {
        // Verica erros enviados pelo express-validator
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            res.status(422).json({ errors: errors.mapped() });
            return;
        }

        const data = Object.assign({}, req.body) || {};
        const uid = req.params.uid;

        const user = await admin.auth().updateUser(uid, {
            displayName: data.displayName,
            phoneNumber: data.phoneNumber,
            customUserClaims: [{
                cpfCnpj: data.cpfCnpj,
                biografia: data.biografia,
                perfil: {
                    status: data.status,
                    descryption: data.descryption
                }
            }]
        });

        res.status(200).send(user, { message: "Usuário atualizado com sucesso." });
    } catch (error) {
        console.error(error);
        if (error.message) {
            res
                .status(404)
                .json({ message: `Problemas ao atualizar registro: ${error.message}` });
        }
        res.status(404).json({ message: ` ${error} ` });
    }
};

let findByUid = async (req, res) => {

    try {
        const uid = req.authId;
        const user = await admin.auth().getUser(uid);
        let usuario = { ...user }

        res.status(200).send(usuario);
    } catch (error) {
        console.error(error);
        if (error.message) {
            res.status(404).json({
                message: `Problemas ao consulta o registro: ${error.message}`,
            });
        }
        res.status(404).json({ message: ` ${error} ` });
    }
};

let validate = (method) => {
    switch (method) {
        case "register":
            {
                return [
                    check("displayName", "Nome é obrigatório.").not().isEmpty(),
                    check("password", "Senha é obrigatório.").not().isEmpty(),
                    check("email").custom((value) => {
                        if (value && !mailer.isEmail(value)) {
                            return Promise.reject(
                                "E-mail informado não é valido. Favor verificar."
                            );
                        }
                        return true;
                    }),
                ];
            }
        case "update":
            {
                return [
                    check("displayName", "Nome é obrigatório.").not().isEmpty(),
                    check("cpfCnpj", "CPF/CNPJ é obrigatório.").not().isEmpty(),
                    check("biografia", "Biografia é obrigatório.").not().isEmpty()
                ];
            }
        case "authenticate":
            {
                return [
                    check("email").custom((value) => {
                        if (value && !mailer.isEmail(value)) {
                            return Promise.reject(
                                "E-mail informado não é valido. Favor verificar."
                            );
                        }
                        return true;
                    }, check("password", "Senha é obrigatório.").not().isEmpty()),
                ];
            }
    }
};

module.exports = {
    authenticate: authenticate,
    register: register,
    update: update,
    findByUid: findByUid,
    validate: validate,
};