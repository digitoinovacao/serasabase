require("dotenv").config();

var express = require('express');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const bodyParser = require("body-parser");
var createError = require('http-errors');
var path = require('path');

var indexRouter = require('./routes/index');
var authenticateRouter = require("./routes/authenticate-route");
var usersRouter = require('./routes/users-route');
var productsRouter = require('./routes/products-route');

var app = express();
var cors = require("cors");

const BASE_PATH = "/api/v1";

app.use(cors());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(`${BASE_PATH}/`, indexRouter);
app.use(`${BASE_PATH}/auth`, authenticateRouter);
app.use(`${BASE_PATH}/users`, usersRouter);
app.use(`${BASE_PATH}/products`, productsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;