Solução que ofereçe armazenamento, processamento e disponibilização desses dados, sempre considerando que tudo deve estar conforme as boas práticas de segurança em TI.

O Projeto se basei em uma arquitetura de micro serviços, onde com os principais pontos:

Payloads Test: Postman
Front End: Angular/Flutter
Authentication: firebase(JWT)
Backend: NodeJS
Data Base: Firestore database
Repositório de Código: GitLab

1 - O Front end envia um Paylod de request, requesição post para poder dá mais segurança 
no trafego das informações. Mesmo sendo uma consulta achei interessante enviar via requisição POST.

2 - Para que a pessoa consiga fazer uma é necessário que ele esteja autenticado, em cada requisiação
no Header de enviar no Authoziathion Bearer {{TOKEN}}, para o cadastro do usuário eu utilizei o firebase 
e um serviço do google que possibilita varios tipos de autenticação. 

3 - O registro do usuário pode ser enviado via front end, ou utilizar o Google ou Facebook necessário ativar no 
firebase. 

4 - o Backend recebe os dados de consulta, uma observação que dados importantes, para o banco de dados A e C, dados como o CPF e outras informações importantes devem ser criptofrados, o Banco B podemos criar indices ou Views para tornar o acesso e gravações mais rápidos.   

5 - O banco de dados e não relacional Firestore database, tbm do google, banco de dados não relacional. O Banco de dados a comunicação é feita certificado os dados do certificado está no arquivo fb-admin-config.json.
