'use strict';
var admin = require("firebase-admin");
const firebaseSequelizer = require("firestore-sequelizer");

var fbAdminConfig = require("../../config/fb-admin-config.json");

admin.initializeApp({
    credential: admin.credential.cert(fbAdminConfig),
    storageBucket: 'serasabase-1be77.appspot.com"',
    databaseURL: "firebase-adminsdk-17unf@serasabase.iam.gserviceaccount.com"
});

firebaseSequelizer.initializeApp(admin);

module.exports = admin