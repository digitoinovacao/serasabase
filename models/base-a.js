const { defineModel } = require("firestore-sequelizer");
const BaseA = defineModel("basea", {
    cpf: '', // CPF
    name: '', // Nome
    address: '', // Endereço
    debts: [], // Lista de dividas
    createdAt: {
        type: 'timestamp'
    },
    updatedAt: {
        type: 'timestamp'
    }
});

module.exports = BaseA