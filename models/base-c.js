const { defineModel } = require("firestore-sequelizer");
const Basec = defineModel("basec", {
    cpf: '',
    createdAt: {
        type: 'timestamp'
    },
    updatedAt: {
        type: 'timestamp'
    }
});
module.exports = Basec