const { defineModel } = require("firestore-sequelizer");
const BaseB = defineModel("baseb", {
    age: '', //idadae
    listGoods: [], // lista de bens
    address: '', //Endereço
    sourceIncome: '', // Fonte de renda
    createdAt: {
        type: 'timestamp'
    },
    updatedAt: {
        type: 'timestamp'
    }
});

module.exports = BaseB